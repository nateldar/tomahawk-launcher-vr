using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UltimateXR.Manipulation;

public class Player : MonoBehaviour
{
    protected int health = 5;
    public float damageCooldown = 2f;
    private float lastDamageTime;
    public GameObject deathIndicator;
    public UnityEvent onHit;
    public UnityEvent onDeath;

    public UxrGrabber leftGrabber;
    public UxrGrabber rightGrabber;

    public LifeBarManager lifeBar;
    // Start is called before the first frame update
    void Start()
    {
        lastDamageTime = 0f;
        deathIndicator.SetActive(false);
        lifeBar.InitHealth(health);
    }


    public void TakeDamage() {
        if (Time.time-lastDamageTime > damageCooldown) {
            health--;
            lifeBar.UpdateHealth(health);
            onHit.Invoke();
            lastDamageTime = Time.time;
            if ( health <= 0 ) {
                Die();
            }
        }

    }

      public void Die() {
          deathIndicator.SetActive(true);
          onDeath.Invoke();
      }



}
