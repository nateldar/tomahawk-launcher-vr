using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenuManager : MonoBehaviour
{
    public GameObject pausePanel;
    private bool playerDead = false;

    private void Update()
    {
        if (pausePanel.activeSelf) {
            if (OVRInput.GetDown(OVRInput.RawButton.A)) {
                SceneLoadingManager.Instance.ChangeScene("Menu", null);
                Time.timeScale = 1f;
                playerDead = false;
            }
            if (OVRInput.GetDown(OVRInput.RawButton.Start) && !playerDead) {
                pausePanel.SetActive(false);
                Time.timeScale = 1f;
            }
        } else {
            if (OVRInput.GetDown(OVRInput.RawButton.Start)) {
                Pause();
            }
        }
    }

    public void Pause(bool playerDead=false){
        pausePanel.SetActive(true);
        Time.timeScale = 0;
        this.playerDead = playerDead;
    }
    // public void DebugLaunch()
    // {
    //     SceneLoadingManager.Instance.ChangeScene("Menu", null);
    // }


}
