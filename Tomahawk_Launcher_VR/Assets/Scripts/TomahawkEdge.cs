using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using UltimateXR.Manipulation;

public class TomahawkEdge : MonoBehaviour
{
    public GameObject tomahawk;
    public string targetTag = "Target";

    void Start() {
        tomahawk.GetComponent<ParentConstraint>().constraintActive = false;
    }


    void OnTriggerEnter(Collider other) {
      if (!other.isTrigger) { // make the triggers behave as they should
          if (other.tag == targetTag) {
            tomahawk.GetComponent<Rigidbody>().isKinematic = true;
            tomahawk.GetComponent<Rigidbody>().velocity = Vector3.zero;
            tomahawk.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
            if (other.GetComponent<Target>()){
              AttachToTransform(other.GetComponent<Target>().attachTransform);
              if (tomahawk.GetComponent<GrabbableTomahawk>().canDamage) {
                other.GetComponent<Target>().TakeDamage();
                tomahawk.GetComponent<GrabbableTomahawk>().canDamage = false;
              }
            } else {
              AttachToTransform(other.gameObject.transform);
            }
          }
      }

    }

    protected void AttachToTransform(Transform newAttach) {
        ParentConstraint constraint = tomahawk.GetComponent<ParentConstraint>();
        if(constraint.sourceCount == 0){
            ConstraintSource source = new ConstraintSource();
            source.sourceTransform = newAttach;
            source.weight = 1;
            constraint.AddSource(source);
            constraint.constraintActive = true;
        }
    }


}
