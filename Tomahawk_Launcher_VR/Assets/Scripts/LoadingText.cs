using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingText : MonoBehaviour
{
    protected Text textArea;
    protected string baseText="";
    // Start is called before the first frame update
    void Start()
    {
        textArea = GetComponent<Text>();
        baseText = textArea.text;
        transform.parent.gameObject.SetActive(false);
    }

    public void UpdateProgress(int progress) {
        textArea.text = baseText + progress.ToString() + " %";
    }

}
