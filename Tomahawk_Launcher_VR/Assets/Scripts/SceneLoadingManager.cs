using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoadingManager : MonoBehaviour
{
    public static SceneLoadingManager Instance;

    protected AsyncOperation currentOperation;

    private void Awake() {
        if (Instance == null) {
            Instance = this;
            DontDestroyOnLoad(this.gameObject);
        } else if (Instance != this) {
            Destroy(this);
        }
    }


    public IEnumerator LoadScene(string sceneName, LoadingText loadingText) {
        if (currentOperation==null) {
            loadingText?.transform.parent.gameObject.SetActive(true);
            loadingText?.UpdateProgress(0);

            currentOperation = SceneManager.LoadSceneAsync(sceneName);
            while (!currentOperation.isDone) {
                loadingText?.UpdateProgress((int)(100 * currentOperation.progress));
                yield return new WaitForEndOfFrame();
            }
            
            currentOperation = null;
        }

    }
    
    public void ChangeScene(string sceneName, LoadingText loadingText) {
        StartCoroutine(SceneLoadingManager.Instance.LoadScene(sceneName,loadingText));
    }

}
