using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UltimateXR.Avatar;
using UltimateXR.Devices;
using UltimateXR.Core;

public class TomahawkSpawner : MonoBehaviour
{
    public GameObject tomahawkPrefab;
    public Transform spawnLocationRight;
    public Transform spawnLocationLeft;

    public float forwardOffset = 0.05f;
    public float upOffset = 0.1f;

    // Update is called once per frame
    void FixedUpdate()
    {
        if (UxrAvatar.LocalAvatarInput.GetButtonsPressDown(UxrHandSide.Right, UxrInputButtons.Button1)) {
            Instantiate(tomahawkPrefab, ApplyOffset(spawnLocationRight), spawnLocationRight.rotation);
        }
        if (UxrAvatar.LocalAvatarInput.GetButtonsPressDown(UxrHandSide.Left, UxrInputButtons.Button1)) {
            Instantiate(tomahawkPrefab, ApplyOffset(spawnLocationLeft), spawnLocationLeft.rotation);
        }
    }

    private Vector3 ApplyOffset(Transform trans) {
      return trans.position + Vector3.up*upOffset + trans.forward*forwardOffset;
    }

}
