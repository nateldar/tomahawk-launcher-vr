using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnkillableTarget : Target
{

    public override void Die() {
        // because it's unkillable we don't invoke OnDeath event
    }
}
