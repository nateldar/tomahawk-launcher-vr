using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public LoadingText loadingText;

    public void OnClickSceneLoader(string sceneName) {
        SceneLoadingManager.Instance.ChangeScene(sceneName,loadingText);
    }

    public void OnClickQuit() {
        Application.Quit();
    }

}
