using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Target
{
    public float maxDistance=15f;
    public float destinationTolerance=1f;
    public float walkSpeed=1f;
    public float runSpeed=1f;
    public string playerTag = "Player";
    public float attackCooldown = 2f;
    protected UnityEngine.AI.NavMeshAgent agent;
    protected Vector3 target;
    protected GameObject player;
    protected float lastAttackTime = 0f;
    private bool inFrontOfPlayer;


    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        target = FindNextDestination();
        agent.SetDestination(target);
        agent.speed = walkSpeed; // initialize NavMeshAgent
        animator = GetComponent<Animator>();
        animator.SetFloat("Speed",walkSpeed);// initialize Animator
        player = GameObject.FindGameObjectsWithTag(playerTag)[0];
        inFrontOfPlayer = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(!animator.GetBool("Dead")) {
            if (inFrontOfPlayer) {
                agent.ResetPath();
                animator.SetFloat("Speed",0f);
            } else {
                if ((this.transform.position-player.transform.position).sqrMagnitude < maxDistance*maxDistance) {
                      target = player.transform.position;
                      agent.SetDestination(player.transform.position);
                      agent.speed = runSpeed;
                      animator.SetFloat("Speed",runSpeed);
                } else {
                      if ((target-this.transform.position).sqrMagnitude < destinationTolerance) {
                          target = FindNextDestination();
                          agent.SetDestination(target);
                          agent.speed = walkSpeed;
                          animator.SetFloat("Speed",walkSpeed);
                      }
                }
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (!animator.GetBool("Dead")) {
            if (other.CompareTag("Player")) {
                inFrontOfPlayer = true;
                if (Time.time > lastAttackTime + attackCooldown) {
                    Attack();
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player")) {
            inFrontOfPlayer = false;
        }
    }

    public void Attack() {
      animator.SetTrigger("Attack");
      lastAttackTime = Time.time;
    }

    protected Vector3 FindNextDestination() {
        Vector2 randomPos2D = Random.insideUnitCircle * maxDistance;
        Vector3 randomPos3D = this.transform.position + new Vector3(randomPos2D.x,0f,randomPos2D.y);
        UnityEngine.AI.NavMeshHit hit;
        UnityEngine.AI.NavMesh.SamplePosition(randomPos3D, out hit, maxDistance, UnityEngine.AI.NavMesh.AllAreas);
        return hit.position;
    }
}
