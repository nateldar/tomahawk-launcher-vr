using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UltimateXR.Manipulation;
using System.Linq;

public class OutlineManager : MonoBehaviour
{
    private UxrGrabber leftHand;
    private UxrGrabber rightHand;
    private UxrGrabbableObject leftTarget;
    private UxrGrabbableObject rightTarget;

    public Color outlineColor = Color.red;

    // Start is called before the first frame update
    void Start()
    {
        leftHand = GetComponent<Player>().leftGrabber;
        rightHand = GetComponent<Player>().rightGrabber;
    }

    // Update is called once per frame
    void Update()
    {
        float minRightDist = Mathf.Infinity;
        float minLeftDist = Mathf.Infinity;

        UxrGrabbableObject newLeftTarget = null;
        UxrGrabbableObject newRightTarget = null;
        List<UxrGrabbableObject> grabbableList = FindObjectsOfType<UxrGrabbableObject>().ToList();

        foreach(UxrGrabbableObject grabbable in grabbableList) {
            if ((grabbable.transform.position - leftHand.transform.position).sqrMagnitude < minLeftDist) {
                newLeftTarget = grabbable;
                minLeftDist = (grabbable.transform.position - leftHand.transform.position).sqrMagnitude;
            }
            if ((grabbable.transform.position - rightHand.transform.position).sqrMagnitude < minRightDist) {
                newRightTarget = grabbable;
                minRightDist = (grabbable.transform.position - rightHand.transform.position).sqrMagnitude;
            }
        }

        if (leftTarget != newLeftTarget) {
            if (leftTarget) {
                leftTarget.GetComponent<Outline>().enabled = false;
            }

            leftTarget = newLeftTarget;
        }

        if (rightTarget != newRightTarget) {
            if (rightTarget) {
                rightTarget.GetComponent<Outline>().enabled = false;
            }    
            rightTarget = newRightTarget;
        }

        if (leftTarget && leftTarget.CanBeGrabbedByGrabber(leftHand,0)) {
            leftTarget.GetComponent<Outline>().enabled = true;
            leftTarget.GetComponent<Outline>().OutlineColor = outlineColor;
        }
        if (rightTarget && rightTarget.CanBeGrabbedByGrabber(rightHand,0)) {
            rightTarget.GetComponent<Outline>().enabled = true;
            rightTarget.GetComponent<Outline>().OutlineColor = outlineColor;
        }

    }
}
