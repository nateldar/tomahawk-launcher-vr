using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Target : MonoBehaviour
{
    public int health = 3;
    public Animator animator;
    public Transform attachTransform;
    public UnityEvent OnDeath;
    public UnityEvent OnHit;

    void Start() {
        if (attachTransform == null) {
          attachTransform = this.transform;
        }
    }

    public void TakeDamage() {
        health--;
        OnHit.Invoke();
        if ( health <= 0 ) {
            Die();
        } else if (animator) {
            animator.SetTrigger("TakeDamages");
        }

    }

    public virtual void Die() {
        if (animator) {
            animator.SetBool("Dead",true);
        }
        GetComponent<Collider>().enabled = false;
        OnDeath.Invoke();
    }

}
