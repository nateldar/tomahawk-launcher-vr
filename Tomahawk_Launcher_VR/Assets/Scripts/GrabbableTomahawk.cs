using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OculusSampleFramework;
using UnityEngine.Animations;
using UltimateXR.Manipulation;

public class GrabbableTomahawk : MonoBehaviour
{
    public float forceMultiplier = 2f;
    private Rigidbody tomahawkBody;
    public bool canDamage = false;
    public float minThrowSpeed = 0.1f;

    private Player player;
    private UxrGrabbableObject grabbable;
    private Outline outline;

    void Start() {
        tomahawkBody = GetComponent<Rigidbody>();
        outline = GetComponent<Outline>();
        grabbable = GetComponent<UxrGrabbableObject>();
        grabbable.Grabbing += OnGrabbing;
        grabbable.Releasing += OnRelease;
        player = GameObject.FindWithTag("Player")?.GetComponent<Player>();
        outline.enabled=false;
    }

    void Update() {

        // if (grabbable.CanBeGrabbedByGrabber(player.leftGrabber,0) || grabbable.CanBeGrabbedByGrabber(player.rightGrabber,0)) {
        //     outline.OutlineMode = Outline.Mode.OutlineAll;
        // } else {
        //     outline.OutlineMode = Outline.Mode.OutlineHidden;
        // }
    }

    private void OnGrabbing(object sender, UxrManipulationEventArgs e){
        ParentConstraint constraint = GetComponent<ParentConstraint>();
        if (constraint.sourceCount > 0) {
            constraint.constraintActive = false;
            constraint.RemoveSource(0);
            canDamage = false;
        }
    }
    private void OnRelease(object sender, UxrManipulationEventArgs e){
        canDamage=true;
    }
}
