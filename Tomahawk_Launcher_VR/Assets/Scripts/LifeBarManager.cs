using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LifeBarManager : MonoBehaviour
{
    public GameObject lifeBarPrefab;

    private List<GameObject> lifeBars = new List<GameObject>();

    private int currentHealth = 0;

    public void InitHealth(int maxHealth) {
        foreach (GameObject bar in lifeBars) {
            Destroy(bar);
        }
        lifeBars.Clear();
        for (int i=0; i<maxHealth; i++){
            GameObject bar = Instantiate(lifeBarPrefab, this.transform);
            lifeBars.Add(bar);
        }
        currentHealth = maxHealth;
    }

    public void UpdateHealth(int newHealth) {
        if (newHealth > currentHealth) {
            for (int i=currentHealth; i<newHealth; i++){
                lifeBars[i].GetComponent<Image>().enabled = true;
            }
        } else if (newHealth < currentHealth) {
            for (int i=currentHealth-1; i>=newHealth; i--){
                lifeBars[i].GetComponent<Image>().enabled = false;
            }
        }
    }

}
