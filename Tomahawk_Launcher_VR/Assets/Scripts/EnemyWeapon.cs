using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWeapon : MonoBehaviour
{
    public string playerTag = "Player";

    void OnTriggerEnter(Collider other) {
        if (other.CompareTag(playerTag)) {
            other.GetComponent<Player>().TakeDamage();
        }
    }


}
